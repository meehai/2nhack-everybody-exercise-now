import sys
import os
import numpy as np
import cv2
import matplotlib.pyplot as plt
import seaborn as sns

from neural_wrappers.utilities import tryReadVideo
from argparse import ArgumentParser
from skvideo.io import FFmpegWriter
from tqdm import trange

sys.path.append("video-keypoint-extractor")
from pose_utils import vidGetPoseKeypoints

sys.path.append("EfficientPose")
from main_inference import getModel, annotate_video

from descriptors import getDescriptorType

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("videoPath")
	parser.add_argument("descriptorType")
	args = parser.parse_args()
	args.videoPath = os.path.abspath(os.path.realpath(args.videoPath))
	return args

def videoGetPartsSummary(descriptor):
	body_parts = descriptor.getYticks()
	Means = descriptor.descriptor.mean(axis=0)
	argSort = np.argsort(Means)[::-1]
	Str = "[Video summary]"
	res = {}

	for ix in argSort:
		part = body_parts[ix]
		Mean = Means[ix]
		Str += "\n - %s. Mean: %2.2f" % (part, Mean)
		res[part] = Mean
	
	argSort = np.argsort(np.array(tuple(res.values())))[::-1]
	Keys = np.array(tuple(res.keys()))[argSort]
	Str += "\n--------- Top 3 body parts ------"
	for i in range(3):
		Str += "\n - %s : %2.2f" % (Keys[i], res[Keys[i]])
	return Str

def videoFocusKeypoint(video, keypoints, descriptor, outPath):
	body_parts = descriptor.getYticks()
	Means = np.abs(descriptor.descriptor).mean(axis=0)
	argSort = np.argsort(Means)[::-1]

	# Initialize annotated video
	vcodec = 'libvpx-vp9' #'libx264'
	print("Save path %s" % outPath)
	fps = str(video.fps)
	writer = FFmpegWriter(outPath, inputdict={'-r': fps}, outputdict={'-r': fps, \
		'-bitrate': '-1', '-vcodec': vcodec, '-pix_fmt': 'yuv420p', '-lossless': '1'})

	radius = 5
	thickness = -1
	N = len(descriptor.descriptor)
	# Sort each frame by magnitude of keypoint descriptor. Top 5 get green, rest are red.
	for i in trange(N, desc="[EfficientPose] Writing focus video"):
		frame = video[i]
		kps = keypoints[i]
		numKps = len(kps)
		frameSort = np.argsort(descriptor.descriptor[i])[::-1]

		for j in range(numKps):
			color = (0, 255, 0) if j < 5 else (255, 0, 0)
			kp = tuple(kps[frameSort[j]])
			frame = cv2.circle(frame, kp, radius, color, thickness)

		writer.writeFrame(frame)
	print("Focus video written to %s" % outPath)

def main():
	args = getArgs()
	model = getModel("iii", "EfficientPose/models/pytorch/EfficientPoseIII")

	video = tryReadVideo(args.videoPath, nFrames=None, imgLib="pims")
	keypoints = vidGetPoseKeypoints(video, model)["coordinates"]
	annotate_video(video, keypoints, args.videoPath.replace(".mp4", "_kp_iii.mp4"))

	descriptor = getDescriptorType(args.descriptorType)(video, keypoints)
	descriptor.computeHeatMap(args.videoPath.replace(".mp4", "_descriptor_%s_heatmap.png" % args.descriptorType))
	print(videoGetPartsSummary(descriptor))
	videoFocusKeypoint(video, keypoints, descriptor, args.videoPath.replace(".mp4", "_focus.mp4"))

if __name__ == "__main__":
	main()