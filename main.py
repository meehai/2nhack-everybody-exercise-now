import cv2
import sys
import numpy as np
import matplotlib.pyplot as plt
import time
from argparse import ArgumentParser

from vke.body import vidGetBodyKeypoints, imgGetBodyKeypoints, imageDisplayBodyParts
from media_processing_lib.video import tryReadVideo, tryWriteVideo
from media_processing_lib.image import imgResize

from descriptors import getDescriptorType
from main_match_exercise import vidSaveMatch

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("videoPath")
	parser.add_argument("descriptorType")
	args = parser.parse_args()
	return args

def main():
	args = getArgs()

	video = tryReadVideo(args.videoPath, nFrames=None, vidLib="pims")
	videoKps = vidGetBodyKeypoints(video, "efficient_pose_iii")["coordinates"]
	d1 = getDescriptorType(args.descriptorType)(video, videoKps)

	cap = cv2.VideoCapture(0)
	_, frame = cap.read()
	frame_height, frame_width = frame.shape[:2]
	Step = "regular"
	myFrames, myKps = [], []
	while True:

		# Read frame
		_, readFrame = cap.read()
		readFrame = readFrame[:, 80:-80]
		readFrame = cv2.flip(readFrame, 1)

		img = readFrame[..., ::-1]
		coordinates = imgGetBodyKeypoints(img, "efficient_pose_i")

		Key = cv2.waitKey(1) & 0xFF
		if Key == ord('q'):
			break
		if Key == ord('a'):
			Step = "show"
			prev = 0
			i = 0
		if Key == ord('d'):
			Step = "countdown"
			prev = time.time()
			i = 0

		if Step == "regular":
			annot = imageDisplayBodyParts(img * 0 + 255, coordinates["coordinates"])
			frameCV = np.concatenate([readFrame, annot], axis=1)
			annotFrame0 = imageDisplayBodyParts(video[0] * 0 + 255, videoKps[0])
			frame2 = np.concatenate([video[0][..., ::-1], annotFrame0], axis=1)
			frame2 = imgResize(frame2, height=frameCV.shape[0], width=frameCV.shape[1], interpolation="bilinear")
			frame = np.concatenate([frame2, frameCV], axis=0)
		elif Step == "show":
			time_elapsed = time.time() - prev

			if time_elapsed <= 1 / video.fps:
				continue
			prev = time.time()
			i += 1

			if i == len(video) - 1:
				i = 0
			# if i == 10:
				# Step = "ready"
			annot = imageDisplayBodyParts(img * 0 + 255, coordinates["coordinates"])
			frameCV = np.concatenate([readFrame, annot], axis=1)
			annotFrame = imageDisplayBodyParts(video[i] * 0 + 255, videoKps[i])
			frame2 = np.concatenate([video[i][..., ::-1], annotFrame], axis=1)
			frame2 = imgResize(frame2, height=frameCV.shape[0], width=frameCV.shape[1], interpolation="bilinear")
			frame = np.concatenate([frame2, frameCV], axis=0)
		elif Step == "ready":
			annot = imageDisplayBodyParts(img * 0 + 255, coordinates["coordinates"])
			frameCV = np.concatenate([readFrame, annot], axis=1)
			annotFrame0 = imageDisplayBodyParts(video[0] * 0 + 255, videoKps[0])
			frame2 = np.concatenate([video[0][..., ::-1], annotFrame0], axis=1)
			frame2 = imgResize(frame2, height=frameCV.shape[0], width=frameCV.shape[1], interpolation="bilinear")
			frame = np.concatenate([frame2, frameCV], axis=0)
			size = 1.4
			thickness = 3
			pos = (0, frame_height)
			Str = "Press 'a' to replay. Press 'd' to start."
			frame = cv2.putText(frame, Str, pos, cv2.FONT_HERSHEY_SIMPLEX, size, \
				(255, 0, 0), thickness, cv2.LINE_AA)
		elif Step == "countdown":
			time_elapsed = 5 - int(time.time() - prev)
			annot = imageDisplayBodyParts(img * 0 + 255, coordinates["coordinates"])
			frameCV = np.concatenate([readFrame, annot], axis=1)
			annotFrame0 = imageDisplayBodyParts(video[0] * 0 + 255, videoKps[0])
			frame2 = np.concatenate([video[0][..., ::-1], annotFrame0], axis=1)
			frame2 = imgResize(frame2, height=frameCV.shape[0], width=frameCV.shape[1], interpolation="bilinear")
			frame = np.concatenate([frame2, frameCV], axis=0)
			size = 5
			thickness = 3
			pos = (frame_height, frame_height)
			Str = "%d" % time_elapsed
			frame = cv2.putText(frame, Str, pos, cv2.FONT_HERSHEY_SIMPLEX, size, \
				(255, 0, 0), thickness, cv2.LINE_AA)
			if time_elapsed == 0:
				i = 0
				Step = "go"
				myFrames, myKps = [], []
		elif Step == "go":
			time_elapsed = time.time() - prev

			if time_elapsed <= 1 / video.fps:
				continue
			prev = time.time()
			i += 1

			if i == len(video) - 1:
			# if i == 10:
				Step = "finish"
				i = 0

			annot = imageDisplayBodyParts(img * 0 + 255, coordinates["coordinates"])
			frameCV = np.concatenate([readFrame, annot], axis=1)
			annotFrame = imageDisplayBodyParts(video[i] * 0 + 255, videoKps[i])
			frame2 = np.concatenate([video[i][..., ::-1], annotFrame], axis=1)
			frame2 = imgResize(frame2, height=frameCV.shape[0], width=frameCV.shape[1], interpolation="bilinear")
			frame = np.concatenate([frame2, frameCV], axis=0)

			myFrames.append(img)
			myKps.append(coordinates["coordinates"])

		elif Step == "finish":
			myFrames = np.array(myFrames)
			myKps = np.array(myKps)
			d2 = getDescriptorType(args.descriptorType)(myFrames, myKps)
			similarity = d1.matchPerFrame(d2)
			print("Mean Similarity: %2.3f" % (similarity.mean() * 100))
			print("Global Similarity: %2.3f" % d1.compareGlobalSimilarity(d2))

			tryWriteVideo(myFrames, "frames.mp4", video.fps)
			vidSaveMatch(video, myFrames, videoKps, myKps, similarity)
			exit()

		print(Step, frame.shape)
		cv2.imshow("Camming is fun", frame)

	cap.release()
	cv2.destroyAllWindows()

if __name__ == "__main__":
	main()