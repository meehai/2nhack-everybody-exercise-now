import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from overrides import overrides

from .raw_changes import RawChanges

class Sign(RawChanges):
	def __init__(self, video, keypoints):
		super().__init__(video, keypoints)
		self.descriptor = self.computeDescriptor()

	@overrides
	def computeDescriptor(self):
		height, width = self.video.shape[1 : 3]
		# Scale keypoints to [0 : 1]
		keypoints = self.keypoints / [height, width]
		keypoints = keypoints.reshape(-1, 32)
		
		# Get the gradient of the keypoints (flow) [-1 : 1]
		descriptor = keypoints[1 :] - keypoints[0 : -1]

		descriptor = np.sign(descriptor)

		return descriptor
