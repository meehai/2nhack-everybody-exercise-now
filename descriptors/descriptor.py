import numpy as np
from numpy.linalg import norm
import matplotlib.pyplot as plt
import seaborn as sns

from abc import ABC, abstractmethod
from overrides import overrides

def cosine_similarity(a, b):
	return np.dot(a, b) / (norm(a) * norm(b))

def f(a, b):
	# breakpoint()
	# return (cosine_similarity(a, b) + 1) / 2
	# breakpoint()
	return 1 - (np.linalg.norm(a - b) / 4)


class Descriptor(ABC):
	def __init__(self, video, keypoints):
		assert len(video) == len(keypoints)
		self.body_parts = ["head_top", "upper_neck", "right_shoulder", "right_elbow", "right_wrist", \
			"thorax", "left_shoulder", "left_elbow", "left_wrist", "pelvis", "right_hip", "right_knee", \
			"right_ankle", "left_hip", "left_knee", "left_ankle"]
		self.video = video
		self.keypoints = keypoints
		self.descriptor = None

	def compareGlobalSimilarity(self, other):
		stack1 = np.median(self.descriptor, axis=0)
		stack2 = np.median(other.descriptor, axis=0)
		return f(stack1, stack2)

	def matchPerFrame(self, other):
		d1 = self.descriptor
		d2 = other.descriptor
		N = len(d1)
		# Cut or append with zeros extra descriptor
		Z = np.zeros(d1.shape)
		Z[0 : min(N, len(d2))] = d2[0 : min(N, len(d2))]
		d2 = Z

		d1 = d1.reshape((N, -1))
		d2 = d2.reshape((N, -1))

		res = np.zeros(N)
		for i in range(N):
			_d1 = d1[max(0, i - 7) : min(i + 7, len(d1))]
			_d2 = d2[max(0, i - 25) : min(i + 25, len(d1))]
			stack1 = np.mean(_d1, axis=0)
			stack2 = np.median(_d2, axis=0)
			# stack1 = d1[i]
			# stack2 = d2[i]
			res[i] = f(stack1, stack2)
		res[res != res] = 0
		return res

	@abstractmethod
	def computeHeatMap(self, fileName):
		pass

	@abstractmethod
	def getYticks(self):
		pass

	def computeComparisonHeatMap(self, other, names):
		stack1 = np.median(self.descriptor, axis=0)
		stack2 = np.median(other.descriptor, axis=0)

		plt.figure(figsize=(10, 1))
		sns.heatmap(np.stack([stack1, stack2]), yticklabels=names)
		fileName = "similarity.png"
		plt.savefig(fileName)
		print("[Descriptor::computeComparisonHeatMap] Stored to %s" % fileName)

	def __str__(self):
		StrDesc = "None" if self.descriptor is None else str(self.descriptor.shape)
		return "Generic Descriptor. Video: %s. Keypoints: %s. Descriptor: %s" % \
			(str(video.shape), str(keypoints.shape), StrDesc)

# hypot = np.hypot(descriptor[..., 0], descriptor[..., 1]) / np.sqrt(2)
# hypot = hypot.T
# plt.figure(figsize=(20, 8))
# ax = sns.heatmap(hypot, yticklabels=body_parts)
# ax.set_xlabel("Frame")
# plt.savefig(path)
# plt.close()
# print("Saved descriptor heatmap to %s" % path)

# def videoGetDescriptor(video, keypoints):
# 	assert len(video) == len(keypoints)
# 	keypoints = keypoints.reshape(-1, 32)
# 	# Get the gradient of the keypoints (flow)
# 	descriptor = keypoints[1 :] - keypoints[0 : -1]
# 	# Do a running mean
# 	Ones = np.ones((10, )) / 10
# 	descriptor = [np.convolve(descriptor[:, i], Ones, mode="same") for i in range(descriptor.shape[-1])]
# 	descriptor = np.stack(descriptor, axis=-1)
# 	# Normalize in [-1:1] on each descriptor frame
# 	Rapp = np.abs(descriptor).max(axis=-1, keepdims=True)
# 	descriptor = descriptor / Rapp
# 	descriptor[descriptor != descriptor] = 0
# 	# Reshape back to original shape
# 	descriptor = descriptor.reshape(-1, 16, 2)
# 	return descriptor

def videoGetDescriptor(video, keypoints):
	assert len(video) == len(keypoints)
	# Normalize keypoints to common reference point
	keypoints = keypoints / [video.shape[1], video.shape[2]]
	keypoints = keypoints.reshape(-1, 32)
	N = 10
	Ones = np.ones((N, )) / N
	keypoints = [np.convolve(keypoints[:, i], Ones, mode="same") for i in range(keypoints.shape[-1])]
	keypoints = np.stack(keypoints, axis=-1)

	# breakpoint()
	# Get the gradient of the keypoints (flow)
	descriptor = keypoints[1 :] - keypoints[0 : -1]

	# Min, Max = np.min(descriptor, axis=0, keepdims=True), np.max(descriptor, axis=0, keepdims=True)
	# descriptor = (descriptor - Min) / (Max - Min)
	# descriptor[descriptor != descriptor] = 0.5
	# descriptor = (descriptor - 0.5) * 2
	# Do a running mean

	descriptor = descriptor.reshape(-1, 16, 2)
	return descriptor

def getStack(d1, d2):
	d1 = np.median(d1, axis=0)
	d2 = np.median(d2, axis=0)

	h1 = np.hypot(d1[:, 0], d1[:, 1])
	h2 = np.hypot(d2[:, 0], d2[:, 1])

	stack1 = np.concatenate([d1[:, 0], d1[:, 1], h1])
	stack2 = np.concatenate([d2[:, 0], d2[:, 1], h2])
	return stack1, stack2

def cosine_similarity(a, b):
	return np.dot(a, b) / (norm(a) * norm(b))

def compareDescriptors(d1, d2, names=None):
	stack1, stack2 = getStack(d1, d2)

	if not names is None:
		plt.figure(figsize=(10, 1))
		sns.heatmap(np.stack([stack1, stack2]), yticklabels=names)
		plt.savefig("similarity.png")

	similarity = cosine_similarity(stack1, stack2)
	return similarity