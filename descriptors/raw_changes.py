import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from overrides import overrides

from .descriptor import Descriptor

class RawChanges(Descriptor):
	def __init__(self, video, keypoints):
		super().__init__(video, keypoints)
		self.descriptor = self.computeDescriptor()

	def computeDescriptor(self):
		height, width = self.video.shape[1 : 3]
		# Scale keypoints to [0 : 1]
		keypoints = self.keypoints / [height, width]
		keypoints = keypoints.reshape(-1, 32)
		
		# Get the gradient of the keypoints (flow)
		descriptor = keypoints[1 :] - keypoints[0 : -1]
		return descriptor

	@overrides
	def getYticks(self):
		yticklabels = []
		for part in self.body_parts:
			yticklabels.append("%s_x" % part)
			yticklabels.append("%s_y" % part)
		return yticklabels

	@overrides
	def computeHeatMap(self, fileName):
		plt.figure(figsize=(20, 8))
		ax = sns.heatmap(self.descriptor.T, yticklabels=self.getYticks())
		ax.set_xlabel("Frame")
		plt.savefig(fileName)
		plt.close()
		print("[RawChanges] Saved descriptor heatmap to %s" % fileName)