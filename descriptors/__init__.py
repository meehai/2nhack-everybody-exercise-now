from .raw_changes import RawChanges
from .changes_min_max import ChangesMinMax
from .changes_max_sign_frame import ChangesMaxSignFrame
from .changes_max_sign_global import ChangesMaxSignGlobal
from .angle import Angle
from .angle_max_sign_global import AngleMaxSignGlobal
from .sign import Sign
from .hypot import Hypot
from .hypot_min_max import HypotMinMax
from .combined import HypotAngleChanges

def getDescriptorType(descriptorType):
    return {
        "RawChanges" : RawChanges,
        "ChangesMinMax" : ChangesMinMax,
        "ChangesMaxSignFrame" : ChangesMaxSignFrame,
        "ChangesMaxSignGlobal" : ChangesMaxSignGlobal,
        "Angle" : Angle,
        "AngleMaxSignGlobal" : AngleMaxSignGlobal,
        "Sign" : Sign,
        "Hypot" : Hypot,
        "HypotMinMax" : HypotMinMax,
        "HypotAngleChanges" : HypotAngleChanges
    }[descriptorType]