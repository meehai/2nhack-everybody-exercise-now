import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from overrides import overrides

from .raw_changes import RawChanges

class AngleMaxSignGlobal(RawChanges):
	def __init__(self, video, keypoints):
		super().__init__(video, keypoints)
		self.descriptor = self.computeDescriptor()

	@overrides
	def computeDescriptor(self):
		height, width = self.video.shape[1 : 3]
		# Scale keypoints to [0 : 1]
		keypoints = self.keypoints / [height, width]
		# keypoints = keypoints.reshape(-1, 32)
		
		# Get the gradient of the keypoints (flow) [-1 : 1]
		descriptor = keypoints[1 :] - keypoints[0 : -1]

		angle = np.arctan2(descriptor[..., 1], descriptor[..., 0])
		descriptor = angle / np.pi

		Rapp = np.abs(descriptor).max(axis=1, keepdims=True)
		descriptor = descriptor / Rapp
		descriptor[descriptor != descriptor] = 0

		# Smooth
		# N = max(10, len(self.video) // 20)
		N = 5
		Ones = np.ones((N, )) / N
		descriptor = [np.convolve(descriptor[:, i], Ones, mode="same") for i in range(descriptor.shape[-1])]
		descriptor = np.stack(descriptor, axis=-1)

		return descriptor

	@overrides
	def getYticks(self):
		return self.body_parts
