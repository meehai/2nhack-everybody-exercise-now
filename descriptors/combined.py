import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from overrides import overrides

from .raw_changes import RawChanges
from .hypot_min_max import HypotMinMax
from .angle_max_sign_global import AngleMaxSignGlobal
from .changes_max_sign_frame import ChangesMaxSignFrame

class HypotAngleChanges(RawChanges):
	def __init__(self, video, keypoints):
		self.hypotMinMax = HypotMinMax(video, keypoints)
		self.angleMaxSignGlobal = AngleMaxSignGlobal(video, keypoints)
		self.changesMaxSignFrame = ChangesMaxSignFrame(video, keypoints)
		super().__init__(video, keypoints)
		self.descriptor = self.computeDescriptor()

	@overrides
	def computeDescriptor(self):
		a = self.hypotMinMax.descriptor
		b = self.angleMaxSignGlobal.descriptor
		c = self.changesMaxSignFrame.descriptor
		return np.concatenate([a, b, c], axis=-1)

	@overrides
	def getYticks(self):
		a = list(map(lambda x : "hypot_%s" % x, self.hypotMinMax.getYticks()))
		b = list(map(lambda x : "angle_%s" % x, self.angleMaxSignGlobal.getYticks()))
		c = list(map(lambda x : "changes_%s" % x, self.changesMaxSignFrame.getYticks()))	
		return a + b + c