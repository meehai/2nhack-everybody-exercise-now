import sys
import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from neural_wrappers.utilities import tryReadVideo
from argparse import ArgumentParser
from pathlib import Path

from vke.body import vidGetBodyKeypoints

from descriptors import getDescriptorType

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("baseDir")
	parser.add_argument("descriptorType")
	args = parser.parse_args()
	args.baseDir = os.path.abspath(os.path.realpath(args.baseDir))
	return args

def main():
	args = getArgs()

	vidPaths = []
	for path in Path(args.baseDir).glob("*/*.mp4"):
		path = str(path)
		if path.find("focus") != -1 or path.find("kp") != -1:
			continue
		vidPaths.append(path)
	print(vidPaths)
	names = list(map(lambda x : x.split("/")[-1].split(".")[0], vidPaths))
	descriptors = []
	for path in vidPaths:
		video = tryReadVideo(path, nFrames=None, imgLib="pims")
		kp = vidGetBodyKeypoints(video, "efficient_pose_iii")["coordinates"]
		d = getDescriptorType(args.descriptorType)(video, kp)
		descriptors.append(d)

	N = len(descriptors)
	res = np.zeros((N, N))
	for i in range(N):
		for j in range(i, N):
			# similarity = compareDescriptors(descriptors[i], descriptors[j])
			similarity = descriptors[i].matchPerFrame(descriptors[j]).mean()
			res[i, j] = similarity
			res[j, i] = similarity

	plt.figure(figsize=(20, 8))
	ax = sns.heatmap(res, yticklabels=names, xticklabels=names)
	plt.savefig("match_all_%s.png" % args.descriptorType)

if __name__ == "__main__":
	main()